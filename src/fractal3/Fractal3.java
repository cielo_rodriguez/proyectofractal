package fractal3;

import java.awt.BorderLayout;
import javax.swing.JFrame;

public class Fractal3 {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    JFrame f = new JFrame();//vista
    f.setVisible(true);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.setSize(800, 600);
    f.setLocation(100, 100);
    f.setLayout(new BorderLayout());
    Panel p = new Panel();
    f.getContentPane().add(p, BorderLayout.CENTER);

  }

}
