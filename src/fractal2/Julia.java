package fractal2;

import java.awt.Color;
import java.awt.Graphics;

public class Julia {

  private final double minR = -1.4;
  private final double maxR = -60.4;
  private final double minI = -1;
  private final double maxI = 1;
  private final int iteraciones = 100;
  private final double rC = -0.76;
  private final double iC = 0.084;

  public Julia() {

  }

  public double getFactor(int ancho, int alto) {
    return Math.max((maxR - minR) / ancho, (maxI - minI) / alto);
  }

  public int enConjunto(double xZ, double yZ, double xC, double yC) {
    for (int i = 0; i < iteraciones; ++i) {
      double modulo = Math.sqrt(xZ * xZ + yZ * yZ);
      //Si el punto esta alejado de 2 en el plano no pertenece
      //al conjunto
      if (modulo > 2) {
        return i;
      }
      double xZn = xZ * xZ - yZ * yZ + xC;
      double yZn = 2 * xZ * yZ + yC;
      xZ = xZn;
      yZ = yZn;
    }
    return iteraciones;
  }

  public void dibujar(Graphics g, int ancho, int alto) {
    System.out.println("Dibujar Disco Siegel");
    double factor = getFactor(ancho, alto);
    for (int pX = 0; pX < ancho; pX++) {
      double x = minR + pX * factor;
      for (int pY = 0; pY < alto; pY++) {
        double y = minI + pY * factor;
        int n = enConjunto(x, y, rC, iC);
        if (n == iteraciones) {
          // Pertenece al Conjunto
          g.setColor(Color.CYAN);
        } else {
          // No pertenece y su lejania la define el color
          int color = 12 + 10 * n / iteraciones;
          g.setColor(new Color(0, color, color));
        }
        g.fillRect(pX, pY, 1, 1);
      }
    }
  }
}
