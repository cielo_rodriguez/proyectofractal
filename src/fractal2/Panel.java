package fractal2;

import java.awt.Graphics;
import javax.swing.JPanel;

public class Panel extends JPanel {

  public Panel() {
  }

  @Override
  public void paint(Graphics g) {
    Julia j = new Julia();
    j.dibujar(g, this.getWidth(), this.getHeight());
  }
}
